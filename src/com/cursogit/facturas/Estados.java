package com.cursogit.facturas

public enum Estados {
	Aprobada,
	Pendiente,
	Probada;
}